import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startExistingApplication('com.ada.astrapayupdate', FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('KYC/Field_Masukkan_NIK'), '1234123412341234', 0)

Mobile.setText(findTestObject('KYC/Field_Masukkan Nama Lengkap'), 'UMI FATIMAH', 0)

Mobile.setText(findTestObject('KYC/Field_Masukkan Tempat Lahir'), 'BENGKULU', 0)

Mobile.tap(findTestObject('KYC/Field_Masukkan_Tanggal_Lahir'), 0)

Mobile.tapAndHold(findTestObject('KYC/Text_2007'), 0, 0)

Mobile.setText(findTestObject('KYC/Text_2007'), '1990', 0)

Mobile.tap(findTestObject('KYC/Btn_OK'), 0)

Mobile.tap(findTestObject('KYC/Field_Masukkan Jenis Kelamin'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('KYC/TextJenisKelaminPerempuan'), 0)

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

Mobile.scrollToText('Alamat (Sesuai KTP)', FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('KYC/Field_Masukkan Pekerjaan'), 'Pengangguran', 0)

Mobile.scrollToText('Alamat (Sesuai KTP)', FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('KYC/FIeld_Masukkan Alamat'), 'jalan jalan jalan', 0)

Mobile.scrollToText('RT', FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('KYC/Field Masukkan RT'), '11', 0)

Mobile.scrollToText('RW', FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('KYC/FieldMasukkan RW'), '11', 0)

Mobile.scrollToText('Provinsi', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('KYC/FieldPilih Provinsi'), 0)

Mobile.tap(findTestObject('KYC/Provinsi_Bengkulu'), 0)

Mobile.tap(findTestObject('KYC/Kabupaten'), 0)

Mobile.tap(findTestObject('KYC/Provinsi_Bengkulu'), 0)

Mobile.tap(findTestObject('KYC/Pilih Kecamatan'), 0)

Mobile.tap(findTestObject('KYC/KelGading Cempaka'), 0)

Mobile.scrollToText('SUBMIT', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('KYC/Desa'), 0)

Mobile.tap(findTestObject('KYC/KelCempaka Permai'), 0)

Mobile.tap(findTestObject('KYC/BtnSUBMITActv'), 0)

Mobile.waitForElementPresent(findTestObject('KYC/ValidasiPekerjaan tidak sesuai'), 0)

Mobile.tap(findTestObject('KYC/Btn_OK1'), 0)

