<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Regression for pulsa pascabayar positive and negative cases</description>
   <name>Biller AstraPay Regression</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>37e26df6-e8ee-4a94-91a7-3b14310ec812</testSuiteGuid>
   <testCaseLink>
      <guid>165a617f-e03c-46f1-be29-5264d40e0abb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Angsuran/FIFGROUP/Transaksi pembayaran Angsuran FIFGROUP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>625f7e8f-8c50-403f-83b2-2ba7850922de</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Angsuran/FIFGROUP/Transaksi pembayaran Angsuran FIFGROUP dengan Nomor Kontrak pembayaran Angsuran Terakhir</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>36b24090-8431-4dce-92c0-bccce7a390e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Angsuran/FIFGROUP/Melakukan Transaksi pembayaran Angsuran FIFGROUP dengan Nomor Kontrak tidak terdaftar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c96e934a-2b8d-4f4a-9400-4f72c188079d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Angsuran/ACC/Transaksi pembayaran Angsuran ACC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a725e20-79f3-49d4-94ca-80425f84135c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Angsuran/ACC/Melakukan Transaksi pembayaran Angsuran ACC dengan Nomor Kontrak tidak terdaftar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e67034f5-c4e2-404d-8f9c-ebf9e7ab3928</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Angsuran/maucash/Melakukan Transaksi Pembayaran Angsuran maucash</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cf683175-055d-4f93-b743-96fe27cc0f90</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Angsuran/maucash/Melakukan Transaksi pembayaran maucash dengan Nomor Kontrak tidak terdaftar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9563d844-c7b7-49a5-a4a9-3eea2a6ad9b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Angsuran/TAF/Melakukan Transaksi pembayaran Angsuran TAF</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8ae711ad-e435-4253-8388-0581deaf0798</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Angsuran/TAF/Melakukan Transaksi pembayaran AngsuranTAF dengan Nomor Kontrak tidak terdaftar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>50e93d54-9a71-40ce-a89c-e4a7adf0dd57</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tagihan dan Isi Ulang/Pulsa/Pulsa Prabayar/Transaksi Pembelian Pulsa Prabayar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0214a939-479c-4d9b-a840-da7b6722eab6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tagihan dan Isi Ulang/Pulsa/Pulsa Prabayar/Transaksi Pembelian Pulsa Prabayar dengan input kode operator salah pada nomor telepon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f6bd1b27-4aa4-48ca-98bd-24c95ec642f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tagihan dan Isi Ulang/Pulsa/Pulsa Prabayar/Transaksi Pembelian Pulsa Prabayar dengan menginputkan nomor melalui Buku Telepon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c5fb8048-4cc2-4bb5-b932-880cb9068320</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tagihan dan Isi Ulang/Pulsa/Pulsa Pascabayar/Transaksi Pembelian Pulsa Pascabayar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>10a31077-58c1-447c-9c74-ec7866d4c22e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tagihan dan Isi Ulang/Pulsa/Pulsa Pascabayar/Pembelian Pascabayar dengan input kode operator salah pada nomor telepon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c84c50da-fb49-4bd1-a904-d4cadfabb892</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tagihan dan Isi Ulang/Pulsa/Pulsa Pascabayar/Pembelian Pulsa Pascabayar dengan menginputkan nomor melalui Buku Telepon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>032cd20e-d528-4a44-b07b-bfd1308f39a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tagihan dan Isi Ulang/Paket Data/Transaksi pembelian Paket Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bd693408-f375-44c9-86be-3b23121b56d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tagihan dan Isi Ulang/Paket Data/Transaksi pembelian Paket Data dengan input kode operator salah pada nomor telepon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3a65b4da-e7d0-4971-aa7e-63dda535532f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tagihan dan Isi Ulang/Paket Data/Transaksi pembelian Paket Data dengan menginputkan nomor melalui Buku Telepon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ba513510-6e70-4b80-9cf7-36fe6342a72a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tagihan dan Isi Ulang/Paket Data/Transaksi pembelian Paket Data ketika produk sedang tidak tersedia</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7c7f6d0b-607c-412b-bb26-8f1378ef041d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tagihan dan Isi Ulang/Telkom/Transaksi Pembayaran Telkom</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4077a5a5-2f0d-4f10-883b-dee64b51886a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tagihan dan Isi Ulang/Telkom/Transaksi Pembayaran Telkom dengan menginputkan nomor telepon tidak terdaftar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3b0ad9e7-cffb-4147-a6d8-b6030123486a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tagihan dan Isi Ulang/PDAM/Transaksi Pembayaran PDAM</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa112776-ea37-460f-bb30-51baf2f3376e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tagihan dan Isi Ulang/PDAM/Transaksi Pembayaran PDAM dengan input nomor pelanggan yang tidak terdaftar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0170c3cc-4aa6-4ea9-a4df-9faec0b48347</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tagihan dan Isi Ulang/PDAM/Melakukan Search Provider penyedia layanan PDAM</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e5bc9c0-749f-41da-80c4-320abb67319f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tagihan dan Isi Ulang/payTV/pembayaran tagihan payTV</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>198ba2b1-3a9d-4b48-9447-14e20f059234</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tagihan dan Isi Ulang/payTV/transaksi tagihan payTV dengan nomor pelanggan yang tidak terdaftar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>37016030-1ca9-4732-a88b-6cdfc5d60642</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Asuransi/BPJS/Melakuakan pembayaran BPJS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cbaae3e1-80e6-449b-897d-328f7766f821</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Asuransi/BPJS/pembayaran BPJS dengan nomor Virtual Account yang tidak terdaftar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>33079c4a-1958-4f07-ad26-e1cc765c9516</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pajak/PBB/Melakuakan pembayaran PBB</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d4012c01-42c7-4775-ae50-1ba0f07a751b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pajak/PBB/pembayaran PBB dengan nomor Pajak yang tidak terdaftar</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
