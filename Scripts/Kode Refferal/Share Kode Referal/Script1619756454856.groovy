import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startExistingApplication('com.ada.astrapayupdate')

Mobile.tap(findTestObject('Kode Refferal/Melihat Kode Referal/Pengaturan_btn'), 0)

Mobile.tap(findTestObject('Kode Refferal/Melihat Kode Referal/KodeReferal_txt'), 0)

Mobile.tap(findTestObject('Kode Refferal/Share Kode Referal/LihatCaranya_btn'), 0)

Mobile.tap(findTestObject('Kode Refferal/Share Kode Referal/BagikanKodeReferral_btn'), 0)

Mobile.tap(findTestObject('Kode Refferal/Share Kode Referal/TouchScreen_txt'), 0)

Mobile.tap(findTestObject('Kode Refferal/Share Kode Referal/BackToKodeReferal_btn'), 0)

Mobile.tap(findTestObject('Kode Refferal/Melihat Kode Referal/BackToProfile_btn'), 0)

Mobile.tap(findTestObject('Kode Refferal/Melihat Kode Referal/BackToHome_btn'), 0)

