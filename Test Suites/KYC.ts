<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>KYC</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>93325065-00d2-4193-8702-08d327c887d8</testSuiteGuid>
   <testCaseLink>
      <guid>e28acaeb-5478-414d-a1d7-57e64d68e6fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/KYC/Perbaharui data dari Classic ke Preferred dengan mengosongkan semua field inputan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6eed53fe-de93-4644-9919-0713e4d1bc72</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/KYC/Perbaharui data dari Classic ke Preferred Mengisi tanggal lahir dibawah 17 tahun</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b5a71d7d-88f6-4cd3-9ed6-b6f560a9191f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/KYC/Perbaharui data dari Classic ke Preferred Mengisi Pekerjaan tidak sesuai dengan saran dari Aplikasi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b8081e3a-ee7c-445a-97d7-0145ed558f62</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/KYC/Perbaharui data dari Classic ke Preferred</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
