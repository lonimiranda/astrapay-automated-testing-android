<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>validasi_no_belum_terdaftar</name>
   <tag></tag>
   <elementGuidId>81a8a185-e58a-4dd5-a6a5-cac4be56f2a1</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <locator>new UiSelector().resourceId(&quot;com.ada.astrapayupdate:id//md_content&quot;).textContains(&quot;Nomor Anda belum terdaftar.
Klik &quot;Register&quot; untuk melakukan proses registrasi.&quot;)</locator>
   <locatorStrategy>ID</locatorStrategy>
</MobileElementEntity>
