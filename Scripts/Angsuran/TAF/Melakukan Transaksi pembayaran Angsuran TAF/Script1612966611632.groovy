import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper as JsonSlurper

Mobile.startExistingApplication('com.ada.astrapayupdate')

Mobile.tap(findTestObject('ACC/android.widget.TextView0 - Angsuran'), 0)

Mobile.tap(findTestObject('TAF/android.widget.TextView0 - TAF'), 0)

Mobile.setText(findTestObject('TAF/android.widget.EditText0 - Masukkan Nomor Virtual Account'), '1750010005', 0)

Mobile.tap(findTestObject('TAF/android.widget.Button0 - Next'), 0)

Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('ACC/android.widget.Button0 - Pay'), 0)

Mobile.callTestCase(findTestCase('General/PIN dan OTP'), null)

Mobile.tap(findTestObject('ACC/android.widget.Button0 - Submit'), 0)

Mobile.tap(findTestObject('ACC/android.widget.Button0 - View Transaction Detail'), 0)

Mobile.tap(findTestObject('ACC/android.widget.TextView0 - Close'), 0)

Mobile.tap(findTestObject('ACC/android.widget.Button0 - Back to Home'), 0)

Mobile.tap(findTestObject('ACC/android.widget.TextView0 - Riwayat'), 0)

Mobile.tap(findTestObject('TAF/android.widget.TextView0 - TAF (1)'), 0)

Mobile.scrollToText('TOTAL BAYAR')

Mobile.tap(findTestObject('ACC/android.widget.ImageButton0'), 0)

Mobile.tap(findTestObject('ACC/android.widget.ImageButton0'), 0)

Mobile.closeApplication()

