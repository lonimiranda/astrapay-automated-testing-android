import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper as JsonSlurper

Mobile.startExistingApplication('com.ada.astrapayupdate')

Mobile.tap(findTestObject('BPJS Kesehatan/android.widget.TextView0 - Asuransi'), 0)

Mobile.tap(findTestObject('BPJS Kesehatan/android.widget.TextView0 - BPJS'), 0)

Mobile.tap(findTestObject('BPJS Kesehatan/android.widget.TextView0 - BPJS Kesehatan'), 0)

Mobile.setText(findTestObject('BPJS Kesehatan/android.widget.EditText0 - Masukkan Nomor Virtual'), '0001800375939', 0)

Mobile.tap(findTestObject('BPJS Kesehatan/android.widget.Button0 - Next'), 0)

Mobile.tap(findTestObject('BPJS Kesehatan/android.widget.TextView0 - December 2020'), 0)

Mobile.tap(findTestObject('BPJS Kesehatan/android.widget.Button0 - Pay'), 0)

Mobile.setEncryptedText(findTestObject('payTV/android.widget.EditText0'), 'aeHFOx8jV/A=', 0)

response = WS.sendRequest(findTestObject('API check OTP/check OTP biller'))

WS.verifyResponseStatusCode(response, 200)

WS.verifyElementPropertyValue(response, '"status_code"', '200')

String jsonString = response.getResponseBodyContent()

JsonSlurper slurper = new JsonSlurper()

Map parsedJson = slurper.parseText(jsonString)

Mobile.setText(findTestObject('BPJS Kesehatan/android.widget.EditText0 (1)'), parsedJson.otp, 0)

Mobile.tap(findTestObject('BPJS Kesehatan/android.widget.Button0 - Submit'), 0)

Mobile.tap(findTestObject('BPJS Kesehatan/android.widget.Button0 - View Transaction Detail'), 0)

Mobile.tap(findTestObject('BPJS Kesehatan/android.widget.TextView0 - Close'), 0)

Mobile.tap(findTestObject('BPJS Kesehatan/android.widget.Button0 - Back to Home'), 0)

Mobile.tap(findTestObject('BPJS Kesehatan/android.widget.TextView0 - Riwayat'), 0)

Mobile.tap(findTestObject('BPJS Kesehatan/android.widget.TextView0 - BPJS Kesehatan (1)'), 0)

Mobile.tap(findTestObject('BPJS Kesehatan/android.widget.ImageButton0'), 0)

Mobile.tap(findTestObject('BPJS Kesehatan/android.widget.ImageButton0'), 0)

Mobile.closeApplication()

