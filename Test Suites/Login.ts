<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>a1e13bd8-f078-412f-8dfd-8fff7cb33a26</testSuiteGuid>
   <testCaseLink>
      <guid>6ca93720-5a29-44ca-a58f-8de03f477068</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TCL01_Input Nomor 10 Digit, 11 Digit, 12 Digit, dan 13 Digit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>993bd3c4-256a-4134-848e-5b3f9cbb2c1a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TCL02_Input Nomor tidak terdaftar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a61402bb-e42a-4cfa-9be0-0aca7dd1455e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TCL03_Input Nomor terdaftar dan PIN Salah</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>360cd75b-13a7-445f-8dfb-c734e6f53d11</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TCL04_Lupa PIN</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>09e6efe7-612d-467c-8541-ef114b9a40c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TCL05_Input Nomor terblokir</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cd742445-f216-49d1-9fdc-cb708a132c6b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TCL06_Pindah Ke Akun Lain</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>309cf27e-8913-4a13-a9a1-620a3dd52b56</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TCL07_Input Nomor prefix invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>254da5d9-2f75-4796-963e-488ee4c7c1c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TCL08_Input Nomor terdaftar dan PIN dengan Benar</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
