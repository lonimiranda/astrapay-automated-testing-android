import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startExistingApplication('com.ada.astrapayupdate', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Login/Field_Input_No_Hp_Tap'), 0)

Mobile.setText(findTestObject('Login/Field_Input_No_Handphone'), '89741826515', 0)

Mobile.tap(findTestObject('Register/Btn_Next'), 0)

Mobile.tap(findTestObject('Register/Btn_Register'), 0)

Mobile.setText(findTestObject('Register/Input_Nama Lengkap Sesuai KTP'), 'Brillian Doloksaribu', 0)

Mobile.setText(findTestObject('Register/Input_Email'), 'brillian@gmail.com', 0)

Mobile.tap(findTestObject('Register/Input_Tanggal lahir'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Register/Btn_DateOKE'), 0)

Mobile.tap(findTestObject('Register/Btn_Next2'), 0)

Mobile.tap(findTestObject('Register/Btn_Skip'), 0)

Mobile.setText(findTestObject('Register/Input_Pin1'), '111111', 0)

Mobile.setText(findTestObject('Register/Input_Pin2'), '000000', 0)

Mobile.checkElement(findTestObject('Register/Text_PIN tidak sama'), 0)

