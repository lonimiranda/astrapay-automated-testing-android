import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper as JsonSlurper

Mobile.startExistingApplication('com.ada.astrapayupdate')

Mobile.tap(findTestObject('Sesama member/android.widget.TextView0 - Transfer  Topup'), 0)

Mobile.tap(findTestObject('Transfer Bank/Btn_TransferBank'), 0)

Mobile.setText(findTestObject('Transfer Bank/Text_InputNominal'), '10000', 0)

Mobile.tap(findTestObject('Transfer Bank/Btn_PilihAkunBank'), 0)

Mobile.tap(findTestObject('Transfer Bank/Btn_AkunBank'), 0)

Mobile.setText(findTestObject('Transfer Bank/Field_IsiInformasi'), 'testing', 0)

Mobile.setText(findTestObject('Transfer Bank/Field_InpuPin'), '123455', 0)

Mobile.tap(findTestObject('Transfer Bank/Btn_Kirim'), 0)

Mobile.checkElement(findTestObject('Transfer Bank/Text_PinSalah'), 0)

Mobile.tap(findTestObject('Transfer Bank/Btn_Ok_Pin'), 0)

