import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startExistingApplication('com.ada.astrapayupdate')

Mobile.tap(findTestObject('Profil/Ganti Email dengan Email yang sudah terdaftar/Pengaturan_btn'), 0)

Mobile.tap(findTestObject('Profil/Ganti Email dengan Email yang sudah terdaftar/GantiEmail_txt'), 0)

Mobile.tap(findTestObject('Profil/Ganti Email dengan Email yang sudah terdaftar/ChangeEmail_btn'), 0)

Mobile.clearText(findTestObject('Profil/Ganti Email/InputEmail_input'), 0)

Mobile.setText(findTestObject('Profil/Ganti Email dengan Email yang sudah terdaftar/InputEmail_input'), 'indra.younghusband26@gmail.com', 
    0)

Mobile.tap(findTestObject('Profil/Ganti Email dengan Email yang sudah terdaftar/Change_btn'), 0)

Mobile.tap(findTestObject('Profil/Ganti Email dengan Email yang sudah terdaftar/Emailsama_text'), 0)

Mobile.tap(findTestObject('Profil/Ganti Email dengan Email yang sudah terdaftar/Back_btn'), 0)

Mobile.tap(findTestObject('Profil/Ganti Email dengan Email yang sudah terdaftar/BackHome_btn'), 0)

