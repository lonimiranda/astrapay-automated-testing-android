<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>NoTidakTedaftar</name>
   <tag></tag>
   <elementGuidId>d8c02209-0d3a-49e9-a323-287bb56464c4</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <locator>//*[@class = 'android.widget.TextView' and (@text = 'Nomor Anda belum terdaftar.
 Klik &quot;Register&quot; untuk melakukan proses registrasi.' or . = 'Nomor Anda belum terdaftar.
 Klik &quot;Register&quot; untuk melakukan proses registrasi.') and @resource-id = 'com.ada.astrapayupdate:id/tv_error_text']

new UiSelector().resourceId(&quot;com.ada.astrapayupdate:id/textViewDescription&quot;).textContains(&quot;Nomor kontrak tidak ditemukan&quot;)</locator>
   <locatorStrategy>ANDROID_UI_AUTOMATOR</locatorStrategy>
</MobileElementEntity>
